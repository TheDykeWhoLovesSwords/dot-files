#!/usr/bin/env bash
#Installing Packages From Pacman
pacman_pkgs()
{
	echo -e "\n Installing packages from default repositories...........\n"
pkgs=(
	 #Qtile
	 #'feh'                         # Wallpaper Manager 
	 #'lxappearance'                # Theme Utility 
	 'network-manager-applet'      # NM Applet 
	 'networkmanager'              # Network Manager 
	 'pavucontrol'                 # Pulse Audio Control
	 #'python-iwlib'                # Qtile Widget Dependency
         #'python-psutil'               # Qtile Widget Dependency
	 #'qtile'                       # Qtile Window Manager
	 'pipewire-pulse'              # Pipewire compatibility for pa 
	 'ttf-font-awesome'            # Awesome Fonts 
	 #'picom'                       # Compositor 
	 #'rofi'                        # Run Launcher

         # utilities

         'alacritty'                   # Terminal Emulator
	 'android-file-transfer'       # Android File Transfer
	 'android-tools'               # ADB
	 'bitwarden'                   # Password Manager
	 'curl'                        # Downloader
	 'deluge'                      # Torrent Client
	 'gparted'                     # Disk Utility
	 'htop'                        # Process manager
	 'lazygit'                     # Terminal Based Git manager
	 'kvantum'
	 'lib32-gnutls'                # Required for Ubisoft Connect
	 'nautilus'                    # Gnome File manager
	 'nemo'                        # File Manager
	 'neofetch'                    # System Information
	 'neovim'                      # Text Editor
	 'npm'                         # Neovim Dependency 
	 'terminator'                  # Terminal Emulator
	 'timeshift'                   # Backup Ulitity
	 'unrar'                       # Rar compression and decompression
	 'unzip'                       # Zip compression and decompression
	 'wget'                        # Downloader
	# 'yay'                         # AUR helper
	 'zsh-completions'             # Z Shell Completions 
	 'zsh'                         # Zshell

	 # Desktop

	 'latte-dock'                  # Dock based on plasma framework

	 # Development
	 
	 'cmake'                       # Cross platform FOSS make System
	 'maven'

	 # 'codeblocks'                  # Cross Platform C/C++ IDE
	 'gcc'                         # C/C++ Compiler
	 'gedit'                       # Gnome text editor
	 'git'                         # Distributed Version Control system for tracing changes in source code development
	 'nodejs'                      # Javascript Runtime Enviornment
	 'python'                      # Programing language
	 
	 # Gaming

	 'lutris'                      # Open Gaming Platform
	 'playonlinux'                 # GUI for managing windows programs
	 'steam'                       # Valve's Game manager

	 # Internet

	 'discord'                     # Voice and Chat service
	 'telegram-desktop'            # Desktop client for Telegram Messenger
	 
	 # Multimedia

	 'celuloid'                    # Media Player
	 'ffmpeg'
	 'vlc'                         # Media Player
	 'youtube-dl'

	 # Graphics and Media manipulation tools

	 'audacity'                    # FOSS Audio Manipulator
	 'gimp'                        # GNU Image Manipulation program
	 'handbrake'                   # Video Transcoder
	 'kdenlive'                    # FOSS Video Editor
	 'obs-studio'                  # FOSS live streaming and Recording

	 # Productivity

	 'libreoffice-fresh'           # Latest Brach of Libre Office

 )
 for pkg in "${pkgs[@]}"; do
	 echo -e "\n Installing ${pkg} \n"
sudo pacman -S "$pkg" --noconfirm --needed
done

echo -e "All packages from default repositories have beeen installed. \n"
}
install_yay(){
sudo pacman -S --needed base-devel
git clone https://aur.archlinux.org/yay.git
cd yay 
makepkg -si
}


#Installing packages From Aur
aur_pkgs(){
echo -e "\n Installing packages from Arch User Repository...........\n"

apkgs=(
#	 'android-studio'                      # Android App Development
	# 'pcloud-drive'                        # Pcloud Client
         'ttf-nerd-fonts-hack-complete-git'    # Nerd Fonts
         'ttf-ms-fonts'                        # Microsoft Fonts 
         'ttf-meslo-nerd-font-powerlevel10k'   # Powerlevel10k Fonts 
	 'heroic-games-launcher-bin'           # Heroic Game Launcher
#	 'mailspring'                          # Mail Client
	 'otf-inconsolata-g-powerline-git'     # Fonts 
	 #'megasync'                           # Mega Cloud Client 
	 'ocs-url'
	 'librewolf-bin'                       # Fork of Firefox
	 'pa-applet-git'                       # Applet for pipewire
	 
	 #Development
	 'phpactor'
)
for apkg in "${apkgs[@]}"; do
echo -e "\n Installing ${apkg} \n"
yay -S --noconfirm $apkg
done

echo -e "\n All packages from Arch User Repository have been installed. \n"
}

#Installing Nvim Language Servers
nvim_lsp()
{
	
echo -e "\n Installing packages from Neo Vim Language Servers...........\n"
lsp_list=(
	'pyright'
	'bash-language-server'
	'vscode-langservers-extracted'
	'typescript'
	'typescript-language-server'
	'yaml-language-server'
	)

	for lsp in "${lsp_list[@]}"; do
		echo -e "\n Installing ${lsp} \n"
		sudo npm install -g $lsp 
	done
	echo -e "All NPM items have been installed"

}
clone_git()
{
	echo -e "\n Cloning Git Repository"
	git clone --depth 1 https://github.com/wbthomason/packer.nvim\  ~/.local/share/nvim/site/pack/packer/start/packer.nvim
	git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/powerlevel10k

}


# Function Calls
pacman_pkgs
install_yay
aur_pkgs
nvim_lsp
clone_git

#Switch shell
chsh -s $(which zsh)
