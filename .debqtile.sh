#!/usr/bin/env bash
repos()
{
echo -e "\n Adding repositories...........\n"

# NALA
echo "deb http://deb.volian.org/volian/ scar main" | sudo tee /etc/apt/sources.list.d/volian-archive-scar-unstable.list
wget -qO - https://deb.volian.org/volian/scar.key | sudo tee /etc/apt/trusted.gpg.d/volian-archive-scar-unstable.gpg > /dev/null
# ZSH completions
echo 'deb http://download.opensuse.org/repositories/shells:/zsh-users:/zsh-completions/xUbuntu_22.04/ /' | sudo tee /etc/apt/sources.list.d/shells:zsh-users:zsh-completions.list
curl -fsSL https://download.opensuse.org/repositories/shells:zsh-users:zsh-completions/xUbuntu_22.04/Release.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/shells_zsh-users_zsh-completions.gpg > /dev/null

#PPA
sudo add-apt-repository ppa:aslatter/ppa -y
sudo add-apt-repository ppa:neovim-ppa/stable -y
sudo add-apt-repository ppa:maarten-fonville/android-studio -y
sudo add-apt-repository ppa:lutris-team/lutris -y
sudo apt update
sudo apt install nala -y

}
# package Installation

installer()
{
echo -e "\n Installing packages...........\n"

pkgs=(
	    # QTILE
	'feh'
	'lxappearance'
	'network-manager-gnome'
	'pavucontrol'
	'python3'
	'python3-pip'
	'python3-psutil'
	'libiw-dev'
	'picom'
	'rofi'
	'volumecontrol.app'

	# Utilities
	'alacritty'
	'android-sdk-platform-tools'
	'bitwarden'
	'curl'
	'gparted'
	'htop'
	'qt5-style-kvantum'
	'wine'
	'nautilus'
	'nemo'
	'neofetch'
	'neovim'
	'npm'
	'timeshift'
	'unrar'
	'unzip'
	'wget'
	'zsh'
	'zsh-completions'

	# Desktop Tools
	'latte-dock'

	# Development
	'cmake'
	'gcc'
	'gedit'
	'git'
	'nodejs'
	'docker'

	# Gaming
	'lutris'
	'steam'

	# Internet
	'telegram-desktop'

	# Multimedia
	'ffmpeg'
	'vlc'
	'youtube-dl'

	# Graphics and Media manipulation tools
	'gimp'
	'handbrake'
	'kdenlive'

	# Productivity
	'libreoffice'
)
for pkg in "${pkgs[@]}"; do
	 echo -e "\n Installing ${pkg} \n"
sudo nala install "$pkg" -y
done

}

font_installer()
{
# MsFonts EULA auto accept
echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | sudo debconf-set-selections

# Fonts
sudo nala install fonts-font-awesome fonts-hack-ttf ttf-mscorefonts-installer -y

cd ~
mkdir fonts
mkdir -p ~/.local/share/fonts
wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf -P ~/fonts/
wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf -P ~/fonts/
wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf -P ~/fonts/
wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf -P ~/fonts/
wget https://github.com/powerline/fonts/raw/master/Inconsolata-g/Inconsolata-g%20for%20Powerline.otf -P ~/fonts/

cp ~/fonts/*ttf ~/.local/share/fonts/
cp ~/fonts/*otf ~/.local/share/fonts/


}

pip()
{
# PIP
pip install iwlib
pip install qtile
}


flatpak()
{
sudo nala install flatpak -y
# add flatpak repo
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# From Flathub
flatpak install flathub com.bitwarden.desktop
flatpak install flathub nz.mega.MEGAsync
flatpak install flathub org.telegram.desktop
flatpak install flathub io.gitlab.librewolf-community

}

# Installing Nvim Language Servers
nvim_lsp()
{

echo -e "\n Installing packages from Neo Vim Language Servers...........\n"
lsp_list=(
	'pyright'
	'bash-language-server'
	'vscode-langservers-extracted'
	'typescript'
	'typescript-language-server'
	'yaml-language-server'
	)

	for lsp in "${lsp_list[@]}"; do
		echo -e "\n Installing ${lsp} \n"
		sudo npm install -g $lsp
	done
	echo -e "All NPM items have been installed"

}

# Clonging Git Repos
clone_git()
{

echo -e "\n Cloning Git Repository"
git clone --depth 1 https://github.com/wbthomason/packer.nvim\
 ~/.local/share/nvim/site/pack/packer/start/packer.nvim
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/powerlevel10k

}

zsh()
{
#Switch shell
chsh -s $(which zsh)
}


#repos
#installer
font_installer
#pip
#flatpak
#nvim_lsp
#clone_git
#zsh





#to be
#lazygit
#discord
#pcloud-drive
#ocs-url
