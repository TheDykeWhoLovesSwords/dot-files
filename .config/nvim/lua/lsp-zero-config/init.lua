require('lsp-zero-config/cmp')
require('lsp-zero-config/signs')
local lsp_zero = require('lsp-zero')

lsp_zero.on_attach(function(client, bufnr)
  -- see :help lsp-zero-keybindings
  -- to learn the available actions
  lsp_zero.default_keymaps({buffer = bufnr})
end)
lsp_zero.setup_servers({'pylsp',
'cssls',
'html',
'tsserver',
'bashls',
'clangd',
'angularls',
'tailwindcss',
'vimls',
'volar',
'yamlls',
'java_language_server',
'grammarly',
'psalm',
})

---
-- Replace these language servers
-- with the ones you have installed in your system
---
require('lspconfig').lua_ls.setup({})
require('lspconfig').rust_analyzer.setup({})
