local map=vim.api.nvim_set_keymap
local opts = { noremap = true, silent = true }
-- Setting Leader Keymaps
map('','<Space>','<NOP>', {noremap=true,silent=true})
vim.g.mapleader= "<Space>"
vim.g.maplocalleader= "<Space>"
--Telescope
map('n','<A-f>',':Telescope find_files<CR>',opts)
map('n','<A-r>',':Telescope live_grep<CR>',opts)
--File Explorer
map('n','<A-e>',':NvimTreeToggle<CR>',opts)
--vim.api.nvim_set_keymap('n','<Leader>e',':NvimTreeToggle<CR>',{noremap=true,silent=true})
--vim.api.nvim_set_keymap('n','<Leader>q',':q<CR>',{noremap=true,silent=true})
--Keymaps
map('n','<A-q>',':q<CR>',opts)
map('n','<Up>','<NOP>', opts)
map('n','<Down>','<NOP>', opts)
map('n','<Left>','<NOP>', opts)
map('n','<Right>','<NOP>', opts)
--Window Navigation
map('n','<C-h>','<C-w>h',{silent=true})
map('n','<C-j>','<C-w>j',{silent=true})
map('n','<C-k>','<C-w>k',{silent=true})
map('n','<C-l>','<C-w>l',{silent=true})
-- Move text up and down
map("v", "<A-j>", ":m .+1<CR>==", opts)
map("v", "<A-k>", ":m .-2<CR>==", opts)
map("v", "p", '"_dP', opts)
-- Move text up and down
map("n", "<A-j>", "<Esc>:m .+1<CR>==gi", opts)
map("n", "<A-k>", "<Esc>:m .-2<CR>==gi", opts)
-- Move text up and down
map("x", "J", ":move '>+1<CR>gv-gv", opts)
map("x", "K", ":move '<-2<CR>gv-gv", opts)
map("x", "<A-j>", ":move '>+1<CR>gv-gv", opts)
map("x", "<A-k>", ":move '<-2<CR>gv-gv", opts)
