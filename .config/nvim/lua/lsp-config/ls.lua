--LanguageServers
  local cap = require('cmp_nvim_lsp').default_capabilities()
  -- Replace <YOUR_LSP_SERVER> with each lsp server you've enabled.
  require'lspconfig'.html.setup {
    capabilities = cap
  }
  require'lspconfig'.cssls.setup {
  capabilities = cap,
}
  require'lspconfig'.tsserver.setup {
  capabilities = cap,
}
  require'lspconfig'.java_language_server.setup {
    capabilities = cap
}
  require'lspconfig'.bashls.setup {
  capabilities = cap,
}
   require'lspconfig'.yamlls.setup {
  capabilities = cap,
}
   require'lspconfig'.pyright.setup{

  capabilities = cap,
 }
   require'lspconfig'.phpactor.setup{
  capabilities=cap,
   }

