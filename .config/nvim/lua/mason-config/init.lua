require("mason").setup()
require("mason-lspconfig").setup {
    ensure_installed = {
	    "angularls",
	    "bashls",
	    "java_language_server",
	    "grammarly",
	    "psalm",
	    "html",
	    "pylsp",
	    "clangd",
	    "cssls",
	    "lua_ls", 
	    "rust_analyzer",
	    "phpactor",
	    "pyright",
	    "tailwindcss",
	    "vimls",
	    "volar",
	    "yamlls",
	    "kotlin_language_server",
	    
    },
}
