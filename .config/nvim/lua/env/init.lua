--Basics
vim.cmd('filetype plugin indent on')
vim.o.hidden=true
vim.o.splitbelow=true
vim.o.splitright=true
vim.o.termguicolors=true
vim.o.cursorline=true
vim.o.cursorcolumn=true
vim.wo.number=true
vim.o.showmode=false
vim.o.backup=false
vim.o.writebackup=false
vim.o.updatetime=100
vim.o.timeoutlen=100
vim.o.clipboard="unnamedplus"
vim.o.ignorecase=true
vim.wo.wrap=false
vim.o.autoindent=true
vim.bo.autoindent=true





--colour Scheme
vim.g.vscode_style = "dark"
vim.cmd[[colorscheme vscode]]


