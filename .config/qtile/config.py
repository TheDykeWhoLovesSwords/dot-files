import os
import re
import socket 
import subprocess
from libqtile import qtile
    

from libqtile import bar, layout, widget, hook
from libqtile.config import  Group,  Match, Screen, Key,Drag
from libqtile.lazy import lazy
from typing import List  # noqa: F401
from modules.vars import mod, myBrowser,terminal,home


keys = [
    Key([mod], "b", lazy.spawn(myBrowser), desc="Move focus to left"),
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(),
        desc="Move window focus to other window"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(),
        desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(),
        desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(),
        desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawn(home+'/.config/rofi/launchers/colorful/launcher.sh &'),
        desc="Spawn a command using a prompt widget"),

    ]






group_names = [("", {'layout': 'monadtall'}),
               ("", {'layout': 'monadtall'}),
               ("﫯", {'layout': 'monadtall'}),
               ("", {'layout': 'monadtall'}),
               ("", {'layout': 'monadtall'}),
               ("", {'layout': 'monadtall'}),
               ("", {'layout': 'monadtall'}),
               ("理", {'layout': 'monadtall'}),
               ("ﲅ", {'layout': 'floating'})]

groups = [Group(name, **kwargs) for name, kwargs in group_names]

for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))        # Switch to another group
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name))) # Send current window to another group

layout_theme = {"border_width": 2,
                "margin": 8,
                "border_focus": "f43337",
                "border_normal": "1D2330"
                }


layouts = [
    layout.Columns(**layout_theme),
    layout.Max(**layout_theme),
    layout.MonadTall(**layout_theme),
    layout.Floating(**layout_theme),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    font='sans',
    fontsize=12,
    padding=8,
    background="#161720"
)
extension_defaults = widget_defaults.copy()
lesbian_pride= [["#D62900", "#D62900"], 
                ["#FF9B55", "#FF9B55"],
                ["#ffffff", "#ffffff"],
                ["#D461a6", "#D461a6"],
                ["#a50062", "#a50062"]] 
black="#000000"
bar_background="ff000000"
colors = [["#282c34", "#282c34"], # panel background
          ["#3d3f4b", "#434758"], # background for current screen tab
          ["#ffffff", "#ffffff"], # font color for group names
          ["#ff5555", "#ff5555"], # border line color for current tab
          ["#74438f", "#74438f"], # border line color for 'other tabs' and color for 'odd widgets'
          ["#4f76c7", "#4f76c7"], # color for the 'even widgets'
          ["#e1acff", "#e1acff"], # window name
          ["#ecbbfb", "#ecbbfb"]] # backbround for inactive screens
prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())
#wallpaper_accent="#7a5cb6"
wallpaper_accent="#f43337"
bar_bg="#161720"
def icon_settings():
	return {
		'padding': 0,
		'fontsize': 20
	}

def left_arrow(color1, color2):
	return widget.TextBox(
		text = '\uE0B2',
        font='Inconsolata for Powerline',
		background = color1,
		foreground = color2,
		**icon_settings()
	)

def right_arrow(color1, color2):
	return widget.TextBox(
		text = '\uE0B0',
        font='Inconsolata for Powerline',
		background = color1,
		foreground = color2,
		**icon_settings()
	)


def init_widgets_list():
    widgets_list1=[

            widget.Sep(
                       linewidth = 0,
                       padding = 8,
                       foreground='#ffffff', 
                       background =wallpaper_accent,
                       ),


                widget.GroupBox(
                    highlight_method='block',
                    this_current_screen_border=bar_bg,
                    block_border='ff0000',
                    fontsize=14,
                    inactive='ffffff',
                    background=wallpaper_accent,
                    ),
                widget.Prompt(),
                right_arrow(bar_bg,wallpaper_accent),
                widget.WindowName(
                    background=bar_bg,
                    ),
                #left_arrow(bar_bg,wallpaper_accent),
                widget.Chord(
                    chords_colors={
                        'launch': ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                                widget.WidgetBox(
                    font='MesloLGS NF',
                     text_closed="  ",
                     fontsize=20,
                     text_open="  ",
 
                    widgets=[
                     widget.TextBox(
                     font='MesloLGS NF',
                     text="  ",
                     fontsize=20,
                     mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(terminal + ' -e nvim .config/nvim/init.lua')},

                ),
                     widget.TextBox(
                     font='MesloLGS NF',
                     text="  ",
                     fontsize=20,
                     mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(terminal + ' -e nvim .config/qtile/config.py')},

                ),


                    
                    ]
                    ),
                 

               left_arrow(bar_bg,wallpaper_accent),
                widget.CurrentLayoutIcon(
                       custom_icon_paths = [home+'/.config/qtile/icons'],
                       foreground = colors[0],
                       background=wallpaper_accent,
                       padding = 0,
                       scale = 0.7
                       ),

                widget.CurrentLayout(
                        background=wallpaper_accent,
                    ),

             left_arrow(wallpaper_accent,bar_bg),
              widget.TextBox(
                      text = "ﮮ",
                       font='MesloLGS NF',
                       foreground = lesbian_pride[2],
                       background = bar_bg,
                       fontsize=20,
                       padding = 5
                       ),
              
                widget.CheckUpdates(
                       update_interval = 1800,
                       distro = "Arch_checkupdates",
                       font='MesloLGS NF',
                       no_update_string="0 ",
                       display_format = "{updates} ",
                       foreground = colors[2],
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(terminal + ' -e sudo pacman -Syu')},
                     #  background =lesbian_pride[1] 
                       ),

            
               left_arrow(bar_bg,wallpaper_accent),
                widget.TextBox(
                      text = "",
                       font='MesloLGS NF',
                       foreground = lesbian_pride[2],
                       background = wallpaper_accent,
                       fontsize=20,
                       padding = 5
                       ),

               widget.Wlan(
                        background=wallpaper_accent,
                        foreground=lesbian_pride[2],
                        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(terminal + ' -e nmtui')},
                        width=120,
                        format='{essid} {percent:2.0%}'

                        ),

             left_arrow(wallpaper_accent,bar_bg),
              widget.TextBox(
                      text = "墳",
                       font='MesloLGS NF',
                       foreground = lesbian_pride[2],
                       background = bar_bg,
                       fontsize=20,
                       padding = 5
                       ),
              widget.PulseVolume(
                      # measure_mem='G',
                       foreground = lesbian_pride[2],
                       padding = 5,
                       width=48
                       ),

               left_arrow(bar_bg,wallpaper_accent),
             widget.TextBox(
                      text = "",
                       font='MesloLGS NF',
                       foreground = lesbian_pride[2],
                       background = wallpaper_accent,
                       fontsize=20,
                       padding = 5
                       ),


               widget.Clock(
                    format='%A,%b,%d ',
                    font='MesloLGS NF',
                    foreground = colors[2],
                    background =wallpaper_accent,
                       ),

               left_arrow(wallpaper_accent,bar_bg),
    
                 widget.TextBox(
                      text = "",
                       font='MesloLGS NF',
                       foreground = lesbian_pride[2],
                       background = bar_bg,
                       fontsize=20,
                       padding = 5
                       ),
              
                widget.Clock(
                    format='%H:%M ',
                    font='MesloLGS NF',
                    foreground = colors[2],
                    background = bar_bg,
                       ),
                 widget.WidgetBox(
                    font='MesloLGS NF',
                     text_closed="  ",
                     text_open="  ",
                     close_button_location='right',
                    # background=wallpaper_accent,
                     foreground=lesbian_pride[2],
 
                    widgets=[

                     left_arrow(bar_bg,wallpaper_accent),
                      widget.Systray(
                      padding = 5,
                      background=wallpaper_accent,
                      icon_size=16,
                ),
                     widget.Sep(
                       linewidth = 0,
                       padding = 16,
                       foreground='#ffffff', 
                       background =wallpaper_accent,
                       ),

               left_arrow(wallpaper_accent,bar_bg),

                    
                    ]
                    ),
                widget.Sep(
                       linewidth = 0,
                       padding = 8,
                       foreground='#ffffff', 
                       background =black,
                       ),



            ]

    widgets_list2=[
                widget.GroupBox(
                    highlight_method='block',
                    this_current_screen_border=lesbian_pride[0],
                    block_border='ff0000',
                    inactive='ffffff',
                    ),
                widget.Prompt(),
                widget.WindowName(),
                widget.Chord(
                    chords_colors={
                        'launch': ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.Systray(
                      padding = 5,
                      icon_size=16,
                ),                
                widget.Sep(
                       linewidth = 0,
                       padding = 6,
                       foreground='#ffffff', 
                       background =black,
                       ),

                widget.CurrentLayoutIcon(
                       custom_icon_paths = [home+'/.config/qtile/icons'],
                       foreground = colors[0],
                       background = lesbian_pride[0],
                       padding = 0,
                       scale = 0.7
                       ),

                widget.CurrentLayout(
                    background=lesbian_pride[0]),
                
                widget.TextBox(
                       text = " ⟳",
                       padding = 2,
                       foreground = colors[2],
                       background = lesbian_pride[1],
                       fontsize = 14
                       ),

                widget.CheckUpdates(
                       update_interval = 1800,
                       distro = "Arch_checkupdates",
                       no_update_string="Updates: 0 ",
                       display_format = "Updates: {updates} ",
                       foreground = colors[2],
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(terminal + ' -e sudo pacman -Syu')},
                       background =lesbian_pride[1] 
                       ),


             widget.TextBox(
                      text = "gg Vol:",
                       foreground = black,
                       background = lesbian_pride[2],
                       padding = 0
                       ),
              widget.Volume(
                       foreground = black,
                       background =lesbian_pride[2], 
                       padding = 5
                       ),

               widget.NetGraph(
                        background=bar_bg,
                        graph_color=colors[2],
                        border_color=colors[3],
                        fill_color=colors[2],
                        bandwith_type='down',
                        type='line',
                        line_width=1,
                        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(terminal + ' -e nmtui')},
                        ),
    
                widget.Clock(
                    format='%d %b, %H:%M ',
                    foreground = colors[2],
                    background = wallpaper_accent,
                       ),
            ]
            
    return widgets_list1

def init_widgets_screen():
    widgets_screen=init_widgets_list()
    return widgets_screen

screens = [
    Screen(
        top=bar.Bar(
            widgets=init_widgets_screen(),        
            size=20,
            #opacity=0.8
                     ),
    ),
]

 # Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()), ]


cursor_warp = False

floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
])
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

@hook.subscribe.startup_once
def start_once():
    subprocess.call([home + '/.config/qtile/autostart.sh'])

wmname = "LG3D"

